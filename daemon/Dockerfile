FROM ubuntu:latest as builder

ENV MONERO_VERSION=0.13.0.4 MONERO_SHA256=693e1a0210201f65138ace679d1ab1928aca06bb6e679c20d8b4d2d8717e50d6

WORKDIR /root
 
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        bzip2 \
        ca-certificates \
        curl \
  && curl https://downloads.getmonero.org/cli/monero-linux-x64-v$MONERO_VERSION.tar.bz2 -O \
  && echo "$MONERO_SHA256  monero-linux-x64-v$MONERO_VERSION.tar.bz2" | sha256sum -c - \
  && tar -xjvf monero-linux-x64-v$MONERO_VERSION.tar.bz2 \
  && rm monero-linux-x64-v$MONERO_VERSION.tar.bz2 \
  && cp ./monero-v$MONERO_VERSION/monerod . \
  && cp ./monero-v$MONERO_VERSION/monero-wallet-rpc . \
  && cp ./monero-v$MONERO_VERSION/monero-wallet-cli . \
  && rm -rf monero-v$MONERO_VERSION
  
FROM ubuntu:latest

WORKDIR /opt/monero

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        gnupg \
        software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get install -y nodejs \
    && npm install -g pm2 \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
  
COPY --from=builder /root/ /opt/monero/

EXPOSE 18081 18082

CMD ["pm2-docker", "process.json"]
